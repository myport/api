const express = require('express');
const router = express.Router();

// staff clint webfontend
const staff= require('../Models/staff.model')
router.get("/staffprofile",staff.profile)
router.post("/register",staff.register)
router.put("/edit/:_id",staff.editprofile)
router.post("/sing",staff.login)
module.exports = router;
