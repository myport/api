const express = require('express');
//const createError = require('http-errors');

const bodyParser = require("body-parser");
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))



const EmegencyRoute = require('./Routes/Emergency.route');
app.use('/api/emergency/', EmegencyRoute);
const staff = require('./Routes/staff');
app.use('/api/staff/', staff);
//404 handler and pass to error handler
app.use((req, res, next) => {
  
  const err = new Error('Not found');
  err.status = 404;
  next(err);
  
  // You can use the above code if your not using the http-errors module
  //next(createError(404, 'Not found'));
});

//Error handler
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.send({
    error: {
      status: err.status || 500,
      message: err.message
    }
  });
});

const PORT = 8081;

app.listen(PORT, () => {
  console.log('Server started on port ' + PORT + '...')
});
