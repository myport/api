const createError = require('http-errors');

const monk = require('monk');
var db = monk("mongodb://localhost:27017/DBemergency");
const jwt = require('jsonwebtoken');


module.exports = {
    register: async (req, res, next) => {
        var obj = {
            first_name:req.body.first_name,
            last_name:req.body.last_name,
            username:req.body.username,
            email:req.body.email,
            gender:req.body.gender,
            staff_image:req.body.staff_image,
            password:req.body.password,
            phone_number:req.body.phone_number
        }
      try {
        db.get("tableStaff").insert(obj , (err,result)=>{
          if (err) throw err;
          if(!result){
            res.json({
              "status": true,
              "message" : "insert Suscess"
            })
          }else{
            res.json({
              "status": false,
              "message" : "insert fail"
            })
          }
        })
      } catch (error) {
        console.log(error.message);
        if (error.name === 'ValidationError') {
          next(createError(422, error.message));
          return;
        }
        next(error);
      }
    },
    login: async (req, res, next) => {
      try {
        db.get("tableStaff").findOne({username:req.body.username,password:req.body.password},function(err, result) {
          if (err) throw err;
        if(result._id !== null){
          const token = jwt.sign(
            { userId: result._id },
            'RANDOM_TOKEN_SECRET',
            { expiresIn: '24h' });
          res.status(200).json({
            userId: result._id,
            token: token
          });
        }else{
          res.status(401).json({
            error: new Error('username or password fild!')
          });
        }
          db.close();
        });
  
      } catch (error) {
        console.log(error.message);
      }
    },  
    
         
    profile: async (req, res, next) => {
      try {
        db.get("tableStaff").find({},function(err, result) {
          if (err) throw err;
          res.json(result);
          db.close();
        });
  
      } catch (error) {
        console.log(error.message);
      }
    },

    editprofile: async (req, res, next) => {
      var obj = {
        first_name:req.body.first_name,
        last_name:req.body.last_name,
        username:req.body.username,
        email:req.body.email,
        gender:req.body.gender,
        staff_image:req.body.staff_image,
        password:req.body.password,
        phone_number:req.body.phone_number
    }
    try {
        db.update({_id:req.body.id},{obj},(err ,result)=>{
          if (err) throw err;
          if(!result){
            res.json({
              "status": true,
              "message" : "update Suscess"
            })
          }else{
            res.json({
              "status": false,
              "message" : "update fail"
            })
          }
        })
    } catch (error) {
      console.log(error.message);
      next(error);
    }
    },
    editprofile: async (req, res, next) => {
      var obj = {
        first_name:req.body.first_name,
        last_name:req.body.last_name,
        username:req.body.username,
        email:req.body.email,
        gender:req.body.gender,
        staff_image:req.body.staff_image,
        password:req.body.password,
        phone_number:req.body.phone_number
    }
    try {
        db.update({_id:req.body.id},{obj},(err ,result)=>{
          if (err) throw err;
          if(!result){
            res.json({
              "status": true,
              "message" : "update Suscess"
            })
          }else{
            res.json({
              "status": false,
              "message" : "update fail"
            })
          }
        })
    } catch (error) {
      console.log(error.message);
      next(error);
    }
    },

};
